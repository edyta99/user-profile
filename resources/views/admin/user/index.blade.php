@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="card">
                    <div class="card-header">Filter</div>
                    <div class="card-body">
                        <form action="{{ route('admin.users.index')}}" method="GET">

                            <div class="form-group row">

                                <div class="col-md-4">
                                    <label for="email"
                                           class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="text" class="form-control" name="email"
                                           value="{{Request::session()->has('User.email') ? Request::session()->get('User.email') : ''}}">
                                </div>
                                <div class="col-md-4">
                                    <label for="name" class="col-form-label text-md-right">Name</label>
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{Request::session()->has('User.name') ? Request::session()->get('User.name') : ''}}">
                                </div>
                                <div class="col-md-4">
                                    <label for="name" class="col-form-label text-md-right">Active</label>
                                    <div class='input-group date'>
                                        {!! Form::select('active', ['' => 'Wybierz', '0' => 'Nieaktywny', '1' => 'Aktywny'], Request::session()->has('User.active') ? Request::session()->get('User.active') : null, ['class' => 'form-control selectpicker', 'id' => 'active']) !!}
                                    </div>
                               </div>
                           </div>
                           <div class="form-group row">
                               <div class="col-md-4">
                                   <div class='form-group'>
                                       <label for="createdAtFrom" class="col-form-label text-md-right">Created at
                                           from</label>
                                       <div class='input-group date'>
                                           {!! Form::date('createdAtFrom', Request::session()->get('User.createdAtFrom') ?? null, ['id' => 'createdAtFrom', 'class' => 'form-control datepicker', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'Date from...']) !!}
                                        </div>
                                    </div>
                                    <div class='form-group'>
                                        <label for="createdAtTo" class="col-form-label text-md-right">Created at to</label>
                                        <div class='input-group date'>
                                            {!! Form::date('createdAtTo', Request::session()->get('User.createdAtTo') ?? null, ['id' => 'createdAtTo', 'class' => 'form-control datepicker', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'Date to...']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class='form-group'>
                                        <label for="updatedAtFrom" class="col-form-label text-md-right">Updated at
                                            from</label>
                                        <div class='input-group date'>
                                            {!! Form::date('updatedAtFrom', Request::session()->get('User.updatedAtFrom') ?? null, ['id' => 'updatedAtFrom', 'class' => 'form-control datepicker', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'Date from...']) !!}
                                        </div>
                                    </div>
                                    <div class='form-group'>
                                        <label for="createdAtTo" class="col-form-label text-md-right">Updated at to</label>
                                        <div class='input-group date'>
                                            {!! Form::date('updatedAtTo', Request::session()->get('User.updatedAtTo') ?? null, ['id' => 'updatedAtTo', 'class' => 'form-control datepicker', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'Date to...']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class='form-group'>
                                        <label for="updatedAtFrom" class="col-form-label text-md-right">Last login at
                                            from</label>
                                        <div class='input-group date'>
                                            {!! Form::date('lastLoginAtFrom', Request::session()->get('User.lastLoginAtFrom') ?? null, ['id' => 'lastLoginAtFrom', 'class' => 'form-control datepicker', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'Date from...']) !!}
                                        </div>
                                    </div>
                                    <div class='form-group'>
                                        <label for="createdAtTo" class="col-form-label text-md-right">Last login at to</label>
                                        <div class='input-group date'>
                                            {!! Form::date('lastLoginAtTo', Request::session()->get('User.lastLoginAtTo') ?? null, ['id' => 'lastLoginAtTo', 'class' => 'form-control datepicker', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'Date to...']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center">
                                <button type="submit" class="btn btn-success">Filter</button>
                            </div>
                        </form>
                        <form action="{{ route('admin.users.clearFilters')}}" method="post">
                            <div class="row justify-content-center">
                                <input type="submit" class="btn btn-success btn-sm" value="Clear filter"/>
                            </div>
                            @method('post')
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="card">
                    <div class="card-header">Users</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif

                        <table class='table'>
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th colspan="1">Name</th>
                                <th colspan="1">Email</th>
                                <th colspan="1">Status</th>
                                <th colspan="1">Created at</th>
                                <th colspan="1">Updated at</th>
                                <th colspan="1">Deleted at</th>
                                <th colspan="1">Last login at</th>
                                <th colspan="1">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                @php $deleted = $user->deleted_at; @endphp
                                <tr @if($deleted) class='bg-danger'
                                    @elseif($user->active === 1) class='bg-light' @endif>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td> @if($user->active === 1) Aktywny @else Nieaktywny @endif</td>
                                    <td>{{$user->created_at}}</td>
                                    <td>{{$user->updated_at}}</td>
                                    <td>{{$user->deleted_at}}</td>
                                    <td>{{$user->lastlogin_at}}</td>
                                    <td>
                                        @if(!$deleted)
                                            <a href="{{ route('admin.users.edit', $user)}}">
                                                <button type="button" class="btn btn-success btn-sm float-left">Edit
                                                </button>
                                            </a>
                                            <form action="{{route('admin.users.softDelete', $user)}}" method="post">
                                                <input type="submit" class="btn btn-danger btn-sm" value="Soft delete"/>
                                                @method('delete')
                                                @csrf
                                            </form>
                                        @endif
                                        <form action="{{route('admin.users.destroy', $user)}}" method="post">
                                            <input type="submit" class="btn btn-warning btn-sm" value="Hard Delete"/>
                                            @method('delete')
                                            @csrf
                                        </form>
                                        @if($deleted)
                                            <form action="{{ route('admin.users.restore', $user )}}" method="post">
                                                <input type="submit" class="btn btn-success btn-sm" value="Restore"/>
                                                @method('put')
                                                @csrf
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<style>
    tr.blue td {
        font-color: #ADD8E6;
    }
</style>
