@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8">
			<div class="row">
				<div class="col-md-3">
				<a href="{{ route('admin.users.index') }}" title="Back to list"><button class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Go to user list</button></a>
				</div>
				<div class="col-md-3">
				<a href="{{ route('admin.avatars.edit', $user->avatar)}}"><button type="button" class="btn btn-success float-left">Go to avatar {{$user->avatar->id}}</button></a>
				</div>
			</div>
            <div class="card">

                <div class="card-header">Edit user {{$user->name}}</div>

                <div class="card-body">
					<form action="{{ route('admin.users.update', $user)}}" method="POST">
							@csrf
							{{method_field('PUT')}}
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('email') is-invalid @enderror" name="name" value="{{$user->name}}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
						<div class="form-group row">
							<label for="roles" class="col-md-4 col-form-label text-md-right"> Roles </label>


								<div class="col-md-6">
									@foreach($roles as $role)

										<input type='checkbox' name='roles[]' value='{{$role->id}}'
										@if($user->roles->pluck('id')->contains($role->id)) checked @endif>
										<label>{{$role->name}}</label>
									@endforeach
								</div>
						</div>
						<div class="form-group row">
							<label for="active" class="col-md-4 col-form-label text-md-right"> Active </label>

								<div class="col-md-6">
										<input type='radio' name='active' value='1'
										@if($user->active === 1) checked @endif>
										<label>User is active</label>
										<input type='radio' name='active' value='0'
										@if($user->active === 0) checked @endif>
										<label>User is not active</label>
								</div>
						</div>

						<button type="submit" class="btn btn-success">Update</button>
					</form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
