@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
			  <div class="card-header">Filter</div>
			  <div class="card-body">
			  <form action="{{ route('admin.avatars.index')}}" method="GET">

                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="originalName" class="col-form-label text-md-right">Original name</label>
                                <input id="originalName" type="text" class="form-control" name="originalName" value="{{Request::session()->has('Avatar.originalName') ? Request::session()->get('Avatar.originalName') : ''}}">
                            </div>
							<div class="col-md-4">
                                <label for="dispalyName" class="col-form-label text-md-right">Dispaly name</label>
                                <input id="dispalyName" type="text" class="form-control" name="displayName" value="{{Request::session()->has('Avatar.displayName') ? Request::session()->get('Avatar.displayName') : ''}}" >
                            </div>
                            <div class="col-md-4">
                                <span class="input-group-addon">Created at</span>
                                <div class='form-group'>
                                    <label for="createdAtFrom" class="col-form-label text-md-right">Created from</label>
                                    <div class='input-group date'>
                                        {!! Form::date('createdAtFrom', Request::session()->get('Avatar.createdAtFrom') ?? null, ['id' => 'createdAtFrom', 'class' => 'form-control datepicker', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'Date from...']) !!}
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label for="createdAtTo" class="col-form-label text-md-right">Created to</label>
                                    <div class='input-group date'>
                                        {!! Form::date('createdAtTo', Request::session()->get('Avatar.createdAtTo') ?? null, ['id' => 'createdAtTo', 'class' => 'form-control datepicker', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'Date to...']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

						<div class="row justify-content-center">
							<button type="submit" class="btn btn-success">Filter</button>
						</div>
					</form>
					<form action="{{ route('admin.avatars.clearFilters')}}" method="post">
						<div class="row justify-content-center">
							<input type="submit"  class="btn btn-success btn-sm" value="Clear filter" />
						</div>
							@method('post')
							@csrf
					</form>
			  </div>
			</div>
		</div>
	</div>
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">Avatars</div>

                <div class="card-body">
					@if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
					 @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

				 <table class='table'>
					<thead>
					<tr>
						<th scope="col">ID</th>
						<th colspan="1">User ID</th>
						<th colspan="1">Display name</th>
						<th colspan="1">Original name</th>
						<th colspan="1">Created at</th>
						<th colspan="1">Updated at</th>
						<th colspan="1">Deleted at</th>
						<th colspan="1">Actions</th>
					</tr>
					</thead>
					<tbody>
					@foreach($avatars as $avatar)
					@php $deleted = $avatar->deleted_at; @endphp
					<tr @if($deleted) class='bg-danger' @endif>
						<td>{{$avatar->id}}</td>
						<td>
							@php $user = $avatar->user; @endphp
							@if($user)
							<a href="{{ route('admin.users.edit', $avatar->user)}}"><button type="button" class="btn btn-success btn-sm float-left">Go to user {{$avatar->user->id}}</button></a>
							@endif
						</td>
						<td>{{$avatar->display_name}}</td>
						<td>{{$avatar->original_name}}</td>
						<td>{{$avatar->created_at}}</td>
						<td>{{$avatar->updated_at}}</td>
						<td>{{$avatar->deleted_at}}</td>
						<td>
						@if(!$deleted)
							<a href="{{ route('admin.avatars.edit', $avatar)}}"><button type="button" class="btn btn-success btn-sm float-left">Edit</button></a>
							<form action="{{route('admin.avatars.softDelete', $avatar)}}" method="post">
							<input type="submit" class="btn btn-danger btn-sm" value="Soft delete" />
							@method('delete')
							@csrf
							</form>
						@endif
							<form action="{{route('admin.avatars.destroy', $avatar)}}" method="post">
							<input type="submit"  class="btn btn-warning btn-sm" value="Hard Delete" />
							@method('delete')
							@csrf
							</form>
						@if($deleted)
							<form action="{{ route('admin.avatars.restore', $avatar )}}" method="post">
							<input type="submit"  class="btn btn-success btn-sm" value="Restore" />
							@method('put')
							@csrf
							</form>
						@endif
						</td>
					</tr>
					@endforeach

					</tbody>
				</table>
				{{ $avatars->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
