@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
							
        <div class="col-md-8">
			 
			<div class="row">
				<div class="col-md-3">
				<a href="{{ route('admin.avatars.index') }}" title="Back to list"><button class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Go to avatar list</button></a>
				</div>
				<div class="col-md-3">
				<a href="{{ route('admin.users.edit', $avatar->user)}}"><button type="button" class="btn btn-success float-left">Go to user {{$avatar->user->id}}</button></a>
				</div>
			</div>
			
            <div class="card">

                <div class="card-header">Edit avatar {{$avatar->id}}</div>

                <div class="card-body">
					<form action="{{ route('admin.avatars.update', $avatar)}}" method="POST">
							@csrf
							{{method_field('PUT')}}				
                        <div class="form-group row">
                            <label for="displayName" class="col-md-4 col-form-label text-md-right">Display name</label>

                            <div class="col-md-6">
                                <input id="displayName" type="text" class="form-control @error('displayName') is-invalid @enderror" name="displayName" value="{{ $avatar->display_name }}" required  autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Original name</label>

                            <div class="col-md-6">
                                <input id="originalName" type="text" class="form-control @error('originalName') is-invalid @enderror" name="originalName" value="{{$avatar->original_name}}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
						
						
						<button type="submit" class="btn btn-success">Update</button>
					</form>
                </div>
				<br>
				<div class="card">

					<div class="card-header">Upload image</div>

					<div class="card-body">
						@if (session('error'))
							<div class="alert alert-danger" role="alert">
								{{ session('error') }}
							</div>
						@endif
						@if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
						@endif
						<form action="{{ route('admin.avatars.uploaded', $avatar)}}" method="POST" enctype="multipart/form-data">
								@csrf
								{{method_field('PUT')}}				
							
							<div class="form-group row">
								<label for="name" class="col-md-4 col-form-label text-md-right">Upload new image</label>

								<div class="col-md-6">
									<input id="newImage" type="file" class="form-control-file" name="newImage">
								</div>
							</div>
							
							<button type="submit" class="btn btn-success">Update</button>
						</form>
						<br>
							<img src="{{ asset('storage/uploaded/' .$avatar->display_name) }}" width="400" height="235" style="margin:0 auto"/>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
