@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">

		<div class="col-md-8">
			<div class="row">

				<div class="col-md-3">
					<a href="{{ route('user.profiles.edit', $user)}}"><button type="button" class="btn btn-success float-left">Edit my profile</button></a>
				</div>
				<div class="col-md-3">
					<a href="{{ route('user.profiles.upload', $user)}}"><button type="button" class="btn btn-success float-left">Upload file</button></a>
				</div>
			</div>
			<div class="card">

				<div class="card-header">Profile {{$user->name}}</div>

				<div class="card-body">
					<div class="form-group row">

						<div class="col-md-6">
							<div>
                            @if($user->active === 1) Aktywny @else Nieaktywny @endif</div>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

					<div class="col-md-6">
						<div>
								{{$user->email}}
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

					<div class="col-md-6">
						<div>
								{{$user->name}}
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label for="email" class="col-md-4 col-form-label text-md-right">Display name</label>

					<div class="col-md-6">
						<div>
								{{$user->avatar->display_name}}
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label for="name" class="col-md-4 col-form-label text-md-right">Original name</label>

					<div class="col-md-6">
						<div>
								{{$user->avatar->original_name}}
						</div>
					</div>
				</div>
				<img src="{{ asset('storage/uploaded/' .$user->avatar->display_name) }}" width="400" height="235" style="margin:0 auto"/>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
