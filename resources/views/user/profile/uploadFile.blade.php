@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">

		<div class="col-md-8">
			<div class="row">

				<div class="col-md-3">
					<a href="{{ route('user.profiles.show', $user)}}"><button type="button" class="btn btn-success float-left">Go to profile</button></a>
				</div>
			</div>
			<div class="card">

				<div class="card-header">Upload image to user {{$user->name}}</div>

				<div class="card-body">
					@if (session('error'))
					<div class="alert alert-danger" role="alert">
                            {{ session('error') }}
					</div>
                    @endif
					<form action="{{ route('user.profiles.uploaded', $user)}}" method="POST" enctype="multipart/form-data">
							@csrf
							{{method_field('PUT')}}				

						<div class="form-group row">
							<label for="name" class="col-md-4 col-form-label text-md-right">Upload new image</label>

							<div class="col-md-6">
								<input id="newImage" type="file" class="form-control-file" name="newImage">
								</div>
							</div>

							<button type="submit" class="btn btn-success">Update</button>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
	