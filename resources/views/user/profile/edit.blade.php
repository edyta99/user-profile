@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
			
        <div class="col-md-8">
			<div class="row">
				
				<div class="col-md-3">
				<a href="{{ route('user.profiles.show', $user)}}"><button type="button" class="btn btn-success float-left">Go to profile</button></a>
				</div>
			</div>
            <div class="card">

                <div class="card-header">Edit user {{$user->name}}</div>

                <div class="card-body">
					<form action="{{ route('user.profiles.update', $user)}}" method="POST" enctype="multipart/form-data">
							@csrf
							{{method_field('PUT')}}				
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('email') is-invalid @enderror" name="name" value="{{$user->name}}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
				
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Original name</label>

                            <div class="col-md-6">
                                <input id="originalName" type="text" class="form-control @error('originalName') is-invalid @enderror" name="originalName" value="{{ $user->avatar ? $user->avatar->original_name  : ''}}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
							
						<button type="submit" class="btn btn-success">Update</button>
					</form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
