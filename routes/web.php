<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/notifications', 'Admin\AdminNotificationController@getNotifications');

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('checkrole')->group(function(){
	Route::post('/users/clearFilters', 'UserController@clearFilters')->name('users.clearFilters');
	Route::delete('/users/softDelete/{user}', 'UserController@softDelete')->name('users.softDelete');
	Route::put('/users/restore/{user}', 'UserController@restore')->name('users.restore');
	Route::resource('/users', 'UserController', ['except' =>['show', 'create', 'store']]); //

	Route::post('/avatars/clearFilters', 'AvatarsController@clearFilters')->name('avatars.clearFilters');
	Route::delete('/avatars/softDelete/{user}', 'AvatarsController@softDelete')->name('avatars.softDelete');
	Route::put('/avatars/restore/{user}', 'AvatarsController@restore')->name('avatars.restore');
	Route::resource('/avatars', 'AvatarsController');
	Route::put('/avatars/upload/{id}', 'AvatarsController@uploadFile')->name('avatars.uploaded');
	Route::post('/notifications/read', 'AdminNotificationController@markAsRead');
});

Route::namespace('User')->prefix('user')->name('user.')->middleware('checkroleprofile')->group(function(){
	Route::resource('/profiles', 'ProfilesController', ['except' =>['index', 'create', 'store', 'destroy']]);
	Route::get('/profiles/upload/{id}', 'ProfilesController@upload')->name('profiles.upload');
	Route::put('/profiles/upload/{id}', 'ProfilesController@uploadFile')->name('profiles.uploaded');

});
