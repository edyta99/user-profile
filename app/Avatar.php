<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Avatar extends Model
{
	use SoftDeletes; 
	
	protected $dates = ['deleted_at'];
	
    protected $fillable = [
        'display_name', 'original_name',
    ];
	
	public function user() {
		return $this->hasOne('App\User','avatar_id', 'id');
	}
	
}
