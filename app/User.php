<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
	use SoftDeletes;

	protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'avatar_id', 'lastlogin_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

	public function roles() {
		return $this->belongsToMany('App\Role');
	}

	public function avatar() {
		return $this->belongsTo('App\Avatar','avatar_id','id');
	}

	public function hasAnyRoles($roles) {
		if($this->roles()->whereIn('name', $roles)->first())
			return true;
		return false;
	}

	public function hasRole($role) {
		if($this->roles()->where('name', $role)->first())
			return true;
	}
    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'channel.admin.' . $this->id;
    }
}
