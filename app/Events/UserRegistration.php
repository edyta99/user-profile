<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Notifications\UserRegistrationNotification;
use Illuminate\Notifications\Notification;

class UserRegistration implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->sendToAdmin();
    }

    private function sendToAdmin()
    {
        $admins = $this->getAdmins();
        foreach ($admins as $admin)
            $admin->notify( new UserRegistrationNotification( $this->user));
    }

    private function getAdmins()
    {
        return User::whereHas('roles', function ($q){
            $q->where('name', 'admin');
        })->get();
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        $channels = collect();
        $admins = $this->getAdmins();
        foreach ($admins as $admin)
            $channels->add(new PrivateChannel('channel.admin.' . $admin->id));

        return $channels->toArray();
    }

    public function broadcastAs()
    {
        return "UserRegistrationEvent";
    }

    public function broadcastWith()
    {
        return [
            'user' => $this->user,
            'email' => $this->user->email,
        ];
    }
}
