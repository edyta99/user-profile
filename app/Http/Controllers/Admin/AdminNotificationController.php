<?php

namespace App\Http\Controllers\Admin;

use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AdminNotificationController extends Controller
{
    public function getNotifications()
    {
        return Auth::check() ? Auth::user()->unreadNotifications()->get() : [];
    }

    public function markAsRead(Request $request)
    {
        Auth::user()->unreadNotifications()->find($request->id)->MarkAsRead();
        return 'success';
    }
}
