<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Avatar;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;



class AvatarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $listPage = 5;
        $avatars = Avatar::withTrashed();

        if ($request->has('originalName')) {
            $request->session()->put('Avatar.originalName', $request->get('originalName'));
        }
        if ($request->has('displayName')) {
            $request->session()->put('Avatar.displayName', $request->get('displayName'));
        }
        if ($request->has('createdAtFrom')) {
            $request->session()->put('Avatar.createdAtFrom', $request->get('createdAtFrom'));
        }
        if ($request->has('createdAtTo')) {
            $request->session()->put('Avatar.createdAtTo', $request->get('createdAtTo'));
        }
        if (!(empty($request->all()) && !$request->session()->get('Avatar'))) {
            $originalName = $request->session()->get('Avatar.originalName');
            $displayName = $request->session()->get('Avatar.displayName');
            $createdAtFrom = $request->session()->get('Avatar.createdAtFrom');
            $createdAtTo = $request->session()->get('Avatar.createdAtTo');
            if ($originalName) {
                $avatars = $avatars->where('original_name', 'like', "%$originalName%");
            }
            if ($displayName !== null) {
                $avatars = $avatars->where('display_name', 'like', "%$displayName%");
            }
            if ($createdAtFrom !== null) {
                $avatars = $avatars
                    ->where('created_at', '>=', Carbon::createFromFormat('Y-m-d', $createdAtFrom)->startOfDay()->toDateTimeString());
            }
            if ($createdAtTo !== null) {
                $avatars = $avatars
                    ->where('created_at', '<=', Carbon::createFromFormat('Y-m-d', $createdAtTo)->endOfDay()->toDateTimeString());
            }
        }
        $avatars = $avatars->orderBy('id', 'desc')->paginate($listPage);

        return View('admin.avatar.index')->with('avatars', $avatars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Avatar $avatar)
    {
        return view('admin.avatar.edit')->with(['avatar' => $avatar]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Avatar $avatar)
    {
        $avatar->update(['original_name' => $request->originalName, 'display_name' => $request->displayName]);
        return redirect()->route('admin.avatars.index')->with('status', 'Edited avatar with id' . $avatar->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Avatar $avatar)
    {

        $avatar->user->roles()->detach();
        $avatar->user()->forceDelete();
        $avatar->forceDelete();
        return redirect()->route('admin.avatars.index')->with('status', 'Avatar with id ' . $avatar->id . ' was permanently deleted');
    }

    public function softDelete($id)
    {
        $avatar = Avatar::find($id);
        $avatar->user->roles()->detach();
        $avatar->user()->delete();
        $avatar->delete();
        return redirect()->route('admin.avatars.index')->with('status', 'Avatar with id ' . $avatar->id . ' was deleted');
    }

    public function restore($id)
    {

        $avatar = Avatar::withTrashed()->find($id);
        $avatar->restore();

        return redirect()->route('admin.avatars.index')->with('status', 'Avatar with id ' . $id . ' was restored');
    }

    public function clearFilters(Request $request)
    {
        Session::forget('Avatar');

        return redirect()->route('admin.avatars.index')->with(['status' => 'Filters Cleared']);
    }
	
	public function uploadFile(Request $request, $avatar)
    {	
		$validator = Validator::make($request->all(), [
			'newImage' => 'required|image',
		]);

		if($validator->fails()){
			return redirect()->route('admin.avatars.edit', $avatar)->with('error', 'Picture should be loaded');
		}
		
		$avatar = Avatar::find($avatar);
		if($file = $request->file('newImage')){
			$clientName = $file->getClientOriginalName();
			$name = pathinfo($file->store('public/uploaded'), PATHINFO_BASENAME);
			$avatar->update(['original_name' => $clientName, 'display_name' => $name]);
		}
		return redirect()->route('admin.avatars.edit', $avatar)->with('status', 'Uploaded file to user with id' . $avatar->id);
    }
}
