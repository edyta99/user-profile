<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use Session;


class UserController extends Controller
{
	public function _construct()
	{
		$this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$listPage = 5;
		$users = User::withTrashed();

		if($request->has('name')) {
            $request->session()->put('User.name', $request->get('name'));
        }
		if($request->has('email')) {
			$request->session()->put('User.email', $request->get('email'));
        }
        if ($request->has('createdAtFrom')) {
            $request->session()->put('User.createdAtFrom', $request->get('createdAtFrom'));
        }
        if ($request->has('createdAtTo')) {
            $request->session()->put('User.createdAtTo', $request->get('createdAtTo'));
        }
        if ($request->has('updatedAtFrom')) {
            $request->session()->put('User.updatedAtFrom', $request->get('updatedAtFrom'));
        }
        if ($request->has('updatedAtTo')) {
            $request->session()->put('User.updatedAtTo', $request->get('updatedAtTo'));
        }
        if ($request->has('lastLoginAtFrom')) {
            $request->session()->put('User.lastLoginAtFrom', $request->get('lastLoginAtFrom'));
        }
        if ($request->has('lastLoginAtTo')) {
            $request->session()->put('User.lastLoginAtTo', $request->get('lastLoginAtTo'));
        }
        if ($request->has('active')) {
            $request->session()->put('User.active', $request->get('active'));
        }
		if(!(empty($request->all()) && !$request->session()->get('User'))) {

			$name = $request->session()->get('User.name');
			$email = $request->session()->get('User.email');
            $createdAtFrom = $request->session()->get('User.createdAtFrom');
            $createdAtTo = $request->session()->get('User.createdAtTo');
            $updatedAtFrom = $request->session()->get('User.updatedAtFrom');
            $updatedAtTo = $request->session()->get('User.updatedAtTo');
            $lastLoginAtFrom = $request->session()->get('User.lastLoginAtFrom');
            $lastLoginAtTo = $request->session()->get('User.lastLoginAtTo');
            $active = $request->session()->get('User.active');

			if($name !== null) {
                $users = $users->where('name','like',"%$name%");
			}
			if($email !== null) {
                $users = $users->where('email','like',"%$email%");
			}
            if($active !== null) {
                $users = $users->where('active','=',$active);
            }
            if ($createdAtFrom !== null) {
                $users = $users
                    ->where('created_at', '>=', Carbon::createFromFormat('Y-m-d', $createdAtFrom)->startOfDay()->toDateTimeString());
            }
            if ($createdAtTo !== null) {
                $users = $users
                    ->where('created_at', '<=', Carbon::createFromFormat('Y-m-d', $createdAtTo)->endOfDay()->toDateTimeString());
            }
            if ($updatedAtFrom !== null) {
                $users = $users
                    ->where('updated_at', '>=', Carbon::createFromFormat('Y-m-d', $updatedAtFrom)->startOfDay()->toDateTimeString());
            }
            if ($updatedAtTo !== null) {
                $users = $users
                    ->where('updated_at', '<=', Carbon::createFromFormat('Y-m-d', $updatedAtTo)->endOfDay()->toDateTimeString());
            }
            if ($lastLoginAtFrom !== null) {
                $users = $users
                    ->where('lastlogin_at', '>=', Carbon::createFromFormat('Y-m-d', $lastLoginAtFrom)->startOfDay()->toDateTimeString());
            }
            if ($lastLoginAtTo !== null) {
                $users = $users
                    ->where('lastlogin_at', '<=', Carbon::createFromFormat('Y-m-d', $lastLoginAtTo)->endOfDay()->toDateTimeString());
            }
		}

		$users = $users->orderBy('id', 'desc')->paginate($listPage);
        return View('admin.user.index')->with('users', $users);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::all();
		return view('admin.user.edit')->with(
		            ['user' => $user,
                     'roles' => $roles
	            	]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->roles()->sync($request->roles);
		$user->update(['name' => $request->name, 'email' => $request->email, 'active' => $request->active]);
		return redirect()->route('admin.users.index')->with('status', 'Edited user with id' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->roles()->detach();
		//$user->avatar()->forceDelete();
		$user->forceDelete();
		return redirect()->route('admin.users.index')->with('status', 'User with id ' . $user->id . ' was permanently deleted');
    }

	public function softDelete(User $user)
    {
		$user->update(['active' => 0]);
        $user->roles()->detach();
		$user->avatar()->delete();
		$user->delete();
		return redirect()->route('admin.users.index')->with('status', 'User with id ' . $user->id . ' was deleted');
    }

	public function restore($id){

		$user = User::withTrashed()->find($id);
		$user->restore();
		$user->avatar()->restore();
		$user->roles()->attach(Role::where('name', 'user')->first());

		return redirect()->route('admin.users.index')->with('status', 'User with id ' . $id . ' was restored');
	}

	public function clearFilters(Request $request)
	{
        Session::forget('User');

        return redirect()->route('admin.users.index')->with(['status' => 'Filters Cleared']);
	}
}
