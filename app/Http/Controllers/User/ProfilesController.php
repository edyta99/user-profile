<?php


namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Avatar;
use Illuminate\Support\Facades\Validator;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'hh';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {	
		$user = User::find($user);
		return view('user.profile.show')->with(['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $user)
    {
		$user = User::find($user);
        return view('user.profile.edit')->with(['user' => $user]);
    }

	public function upload($user)
    {
		$user = User::find($user);
        return view('user.profile.uploadFile')->with(['user' => $user]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {	
		$user = User::find($user);
        $user->update(['name' => $request->name, 'email' => $request->email]);
		$user->avatar()->update(['original_name' => $request->originalName]);
		return redirect()->route('user.profiles.show', $user)->with('status', 'Edited user with id' . $user->id);
    }

	public function uploadFile(Request $request, $user)
    {	
		$validator = Validator::make($request->all(), [
			'newImage' => 'required|image',
		]);

		if($validator->fails()){
			return redirect()->route('user.profiles.upload', $user)->with('error', 'Picture should be loaded');
		}
		
		$user = User::find($user);
		if($file = $request->file('newImage')){
			$clientName = $file->getClientOriginalName();
			$name = pathinfo($file->store('public/uploaded'), PATHINFO_BASENAME);
			$user->avatar()->update(['original_name' => $clientName, 'display_name' => $name]);
		}
		return redirect()->route('user.profiles.show', $user)->with('status', 'Uploaded file to user with id' . $user->id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
