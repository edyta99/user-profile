<?php

namespace App\Http\Middleware;

use Closure;

class CheckRoleUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(auth()->user() && auth()->user()->hasRole('admin'))
			return $next($request);
		
		return redirect()->route('home')->with('error', 'You have no access');
    }
}
