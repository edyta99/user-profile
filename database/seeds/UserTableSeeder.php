<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Avatar;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//User::truncate();
		DB::table('role_user')->delete();
		DB::table('users')->delete();
		
		
        $adminRole = Role::where('name', 'admin')->first();
		$userRole = Role::where('name', 'user')->first();
		$accessRole = Role::where('name', 'access')->first();
		
		$adminAvatar = Avatar::where('id', 1)->first();
		$userAvaOne = Avatar::where('id', 2)->first();
		$userAvaTwo = Avatar::where('id', 3)->first();
		$userAvaThree = Avatar::where('id', 4)->first();
		$accessAvatar = Avatar::where('id', 5)->first();
		
		$admin = User::create(['name' => 'admin', 'email' => 'admin@admin.pl', 'active' => 1, 'password' => Hash::make('password'), 'avatar_id' => $adminAvatar->id]);
		$userOne = User::create(['name' => 'user one', 'email' => 'userOne@user.pl', 'active' => 1, 'password' => Hash::make('password'), 'avatar_id' => $userAvaOne->id]);
		$userTwo = User::create(['name' => 'user two', 'email' => 'userTwo@admin.pl', 'active' => 1, 'password' => Hash::make('password'),'avatar_id' => $userAvaTwo->id]);
		$userThree = User::create(['name' => 'user three', 'email' => 'userThree@admin.pl', 'active' => 0, 'password' => Hash::make('password'),'avatar_id' => $userAvaThree->id]);
		$accessUser = User::create(['name' => 'user four', 'email' => 'access@admin.pl', 'active' => 1, 'password' => Hash::make('password'),'avatar_id' => $accessAvatar->id]);
		
		$admin->roles()->attach($adminRole);
		$userOne->roles()->attach($userRole);
		$userTwo->roles()->attach($userRole);
		$userThree->roles()->attach($userRole);
		$accessUser->roles()->attach($accessRole);
		
    }
}
