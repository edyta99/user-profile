<?php

use Illuminate\Database\Seeder;
use App\Avatar;

class AvatarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('Avatars')->delete();
		
        Avatar::create(['display_name' => 'TA One', 'original_name' => 'Test avatar One']);
        Avatar::create(['display_name' => 'TA Two', 'original_name' => 'Test avatar Two']);
        Avatar::create(['display_name' => 'TA Three', 'original_name' => 'Test avatar Three']);
		Avatar::create(['display_name' => 'TA Four', 'original_name' => 'Test avatar Four']);
        Avatar::create(['display_name' => 'TA Fife', 'original_name' => 'Test avatar Fife']);
		Avatar::create(['display_name' => 'TA Six', 'original_name' => 'Test avatar Six']);
    }
}
