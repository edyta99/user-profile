<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
		DB::table('Roles')->delete();
		
        Role::create(['name' => 'admin']);
		Role::create(['name' => 'user']);
		Role::create(['name' => 'access']);
    }
}
